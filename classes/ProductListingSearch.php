<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

use PrestaShop\PrestaShop\Adapter\Search\SearchProductSearchProvider;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrderFactory;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrdersCollection;

/**
 * This class is the base class for all front-end "product listing" controllers,
 * like "CategoryController", that is, controllers whose primary job is
 * to display a list of products and filters to make navigation easier.
 */
class ProductListingSearch extends ProductListingFrontController
{

    public $products_var = array();
    public $id_categoria = 1;
    public $context = null;

    public function __construct()
    {
        parent::__construct();
        $this->php_self = 'search';
        $this->context = Context::getContext();
    }


    /**
     * {@inheritdoc}
     */
    public function initContent()
    {
        parent::initContent();

    }
    protected function getProductSearchContext()
    {
        return (new ProductSearchContext())
          ->setIdShop($this->context->shop->id)
          ->setIdLang($this->context->language->id)
          ->setIdCurrency($this->context->currency ?
            $this->context->currency->id :
            null)
          ->setIdCustomer(
            $this->context->customer ?
              $this->context->customer->id :
              null
          )
          ;
    }

    protected function getProductSearchQuery()
    {
        $query = new ProductSearchQuery();
        $query
          ->setQueryType('products')
          ->setSearchString('scatole')
          ->setEncodedFacets(false)
          ->setSearchTag('scatole')
        ;

        return $query;
    }

    protected function getDefaultProductSearchProvider()
    {
        return new SearchProductSearchProvider(
          $this->context->getTranslator()
        );
    }

    public function getListingLabel()
    {
        return $this->trans('Risultati della ricerca:',array(), 'Modules.Op_searchproducts.ProductListingSearch');
    }

    /**
     * Takes an associative array with at least the "id_product" key
     * and returns an array containing all information necessary for
     * rendering the product in the template.
     *
     * @param array $rawProduct an associative array with at least the "id_product" key
     *
     * @return array a product ready for templating
     * @throws ReflectionException
     */
    protected function prepareProductForTemplate(array $rawProduct)
    {
        $product = (new ProductAssembler($this->context))
          ->assembleProduct($rawProduct)
        ;

        $presenter = $this->getProductPresenter();
        $settings = $this->getProductPresentationSettings();

        return $presenter->present(
          $settings,
          $product,
          $this->context->language
        );
    }
    /**
     * Runs "prepareProductForTemplate" on the collection
     * of product ids passed in.
     *
     * @param array $products array of arrays containing at list the "id_product" key
     *
     * @return array of products ready for templating
     */
    public function prepareMultipleProductsForTemplate(array $products)
    {
        return array_map(array($this, 'prepareProductForTemplate'), $products);
    }

    /**
     * This method is the heart of the search provider delegation
     * mechanism.
     *
     * It executes the `productSearchProvider` hook (array style),
     * and returns the first one encountered.
     *
     * This provides a well specified way for modules to execute
     * the search query instead of the core.
     *
     * The hook is called with the $query argument, which allows
     * modules to decide if they can manage the query.
     *
     * For instance, if two search modules are installed and
     * one module knows how to search by category but not by manufacturer,
     * then "ManufacturerController" will use one module to do the query while
     * "CategoryController" will use another module to do the query.
     *
     * If no module can perform the query then null is returned.
     *
     * @param ProductSearchQuery $query
     *
     * @return ProductSearchProviderInterface or null
     */
    private function getProductSearchProviderFromModules($query)
    {
        $providers = Hook::exec(
          'productSearchProvider',
          array('query' => $query),
          null,
          true
        );

        if (!is_array($providers)) {
            $providers = array();
        }

        foreach ($providers as $provider) {
            if ($provider instanceof ProductSearchProviderInterface) {
                return $provider;
            }
        }
    }

    /**
     * This returns all template variables needed for rendering
     * the product list, the facets, the pagination and the sort orders.
     *
     * @return array variables ready for templating
     * @throws PrestaShopException
     * @throws Exception
     */
    public function getProductSearchVariables()
    {
        /*
         * To render the page we need to find something (a ProductSearchProviderInterface)
         * that knows how to query products.
         */

        // the search provider will need a context (language, shop...) to do its job
        $context = $this->getProductSearchContext();
        if ($context->getIdCurrency() == null){
            $context->setIdCurrency($this->context->country->id_currency);
        }

        // the controller generates the query...
        $query = $this->getProductSearchQuery();

        // ...modules decide if they can handle it (first one that can is used)
        $provider = $this->getProductSearchProviderFromModules($query);

        // if no module wants to do the query, then the core feature is used
        if (null === $provider) {
            $provider = $this->getDefaultProductSearchProvider();
        }

        $resultsPerPage = (int) Tools::getValue('resultsPerPage');
        if ($resultsPerPage <= 0) {
            $resultsPerPage = Configuration::get('PS_PRODUCTS_PER_PAGE');
        }

        // we need to set a few parameters from back-end preferences
        $query
          ->setResultsPerPage($resultsPerPage)
          ->setIdCategory($this->id_categoria)
          ->setPage(max((int) Tools::getValue('page'), 1))
        ;

        // set the sort order if provided in the URL
        if (($encodedSortOrder = Tools::getValue('order'))) {
            $query->setSortOrder(SortOrder::newFromString(
              $encodedSortOrder
            ));
        }

        // get the parameters containing the encoded facets from the URL
        $encodedFacets = Tools::getValue('q');

        /*
         * The controller is agnostic of facets.
         * It's up to the search module to use /define them.
         *
         * Facets are encoded in the "q" URL parameter, which is passed
         * to the search provider through the query's "$encodedFacets" property.
         */

        $query->setEncodedFacets($encodedFacets);

        // We're ready to run the actual query!
        $result = new ProductSearchResult();

        if (Configuration::get('PS_CATALOG_MODE') && !Configuration::get('PS_CATALOG_MODE_WITH_PRICES')) {
            $this->disablePriceControls($result);
        }

        //set founded products
        $result
          ->setProducts($this->products_var)
          ->setTotalProductsCount(count($this->products_var));
        $orderFactory = new SortOrdersCollection($this->translator);
        $result->setAvailableSortOrders(
          $orderFactory->getDefaults()
        );


        // sort order is useful for template,
        // add it if undefined - it should be the same one
        // as for the query anyway
        if (!$result->getCurrentSortOrder()) {
            $result->setCurrentSortOrder($query->getSortOrder());
        }

        // prepare the products
        $products = $this->prepareMultipleProductsForTemplate(
          $result->getProducts()
        );

        // render the facets
        if ($provider instanceof FacetsRendererInterface) {
            // with the provider if it wants to
            $rendered_facets = $provider->renderFacets(
              $context,
              $result
            );
            $rendered_active_filters = $provider->renderActiveFilters(
              $context,
              $result
            );
        } else {
            // with the core
            $rendered_facets = $this->renderFacets(
              $result
            );
            $rendered_active_filters = $this->renderActiveFilters(
              $result
            );
        }

        $pagination = $this->getTemplateVarPagination(
          $query,
          $result
        );

        // prepare the sort orders
        // note that, again, the product controller is sort-orders
        // agnostic
        // a module can easily add specific sort orders that it needs
        // to support (e.g. sort by "energy efficiency")
        $sort_orders = $this->getTemplateVarSortOrders(
          $result->getAvailableSortOrders(),
          $query->getSortOrder()->toString()
        );

        $sort_selected = false;
        if (!empty($sort_orders)) {
            foreach ($sort_orders as $order) {
                if (isset($order['current']) && true === $order['current']) {
                    $sort_selected = $order['label'];
                    break;
                }
            }
        }

        $searchVariables = array(
          'result' => $result,
          'label' => $this->getListingLabel(),
          'products' => $products,
          'sort_orders' => $sort_orders,
          'sort_selected' => $sort_selected,
          'pagination' => $pagination,
          'rendered_facets' => $rendered_facets,
          'rendered_active_filters' => $rendered_active_filters,
          'js_enabled' => $this->ajax,
          'current_url' => $this->updateQueryString(array(
            'q' => $result->getEncodedFacets(),
          )),
        );

        Hook::exec('filterProductSearch', array('searchVariables' => &$searchVariables));
        Hook::exec('actionProductSearchAfter', $searchVariables);

        return $searchVariables;
    }

}
