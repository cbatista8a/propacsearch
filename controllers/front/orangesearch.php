<?php

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\ProductPresentationSettings;
include_once(_PS_MODULE_DIR_.'op_searchproducts/classes/ProductListingSearch.php');
if (!defined('_PS_VERSION_')) {
    exit;
}

class op_searchproductsOrangeSearchModuleFrontController extends ModuleFrontController
{
    private $scatole_category;
    private $sacchetti_category;
    private $attribute_group_width;
    private $attribute_group_height;
    private $attribute_group_width_sacchetti;
    private $attribute_group_height_sacchetti;
    private $attribute_group_depth;
    private $attribute_group_dimension;
    private $search_margin;
    protected $context;
    public $product_listing;
    public $last_search_values;
    public $searchs = array();
    public $listing= array();

    public function __construct()
    {
        $this->scatole_category = (int)Configuration::get('CATEGORY_SCATOLE_SEARCH');
        $this->sacchetti_category = (int)Configuration::get('CATEGORY_SACCHETTI_SEARCH');
        $this->attribute_group_width = (int)Configuration::get('ATTRIBUTE_GROUP_WIDTH');
        $this->attribute_group_height = (int)Configuration::get('ATTRIBUTE_GROUP_HEIGHT');
        $this->attribute_group_depth = (int)Configuration::get('ATTRIBUTE_GROUP_DEPTH');
        $this->attribute_group_width_sacchetti = (int)Configuration::get('ATTRIBUTE_GROUP_WIDTH_SACCHETTI');
        $this->attribute_group_height_sacchetti = (int)Configuration::get('ATTRIBUTE_GROUP_HEIGHT_SACCHETTI');
        $this->search_margin = (int)Configuration::get('SEARCH_MARGIN');
        $this->attribute_group_dimension = 3;
        $this->last_search_values = json_decode(Configuration::get('LAST_SEARCH'),true);
        $this->context = Context::getContext();
        $this->product_listing = new ProductListingSearch();
        $this->listing = $this->product_listing->getProductSearchVariables();
        parent::__construct();
    }

    public function initContent()
    {
        $category = 1;
        $tipe_of_search = 'box';
        if (((bool)Tools::isSubmit('form_box')) == true){
            $category = $this->scatole_category;
        }elseif (((bool)Tools::isSubmit('form_bags')) == true){
            $category = $this->sacchetti_category;
            $tipe_of_search = 'bags';
        }

        $tipo = Tools::getValue('select_tipo');
        $larghezza = Tools::getValue('tipo_width');
        $lunghezza = Tools::getValue('tipo_height');
        $altezza = Tools::getValue('tipo_depth');
        $unit = Tools::getValue('select_unit');

        try {
            $this->searchs = $this->search($tipe_of_search,$category,$tipo, $larghezza, $lunghezza, $altezza, $unit); // If you want to use the Prestashop default attribute for the dimensions, change to the Legacy method for the search.
        } catch (PrestaShopDatabaseException $e) {
        }

        if (!empty($this->searchs)){
            $this->last_search_values = array('tipe_of_search'=>$tipe_of_search,'category'=>$category,'tipo' => $tipo, 'larghezza' => $larghezza, 'lunghezza' => $lunghezza, 'altezza' => $altezza, 'unit' => $unit);
            Configuration::updateValue('LAST_SEARCH',json_encode($this->last_search_values));
            try {
                $this->product_listing->products_var = $this->searchs;
                $this->product_listing->id_categoria = $category;
                $this->listing = $this->product_listing->getProductSearchVariables();
            } catch (PrestaShopException $e) {
            }
        }
        if (((bool)Tools::isSubmit('order')) == true){
            if (empty($this->last_search_values))
                $this->last_search_values = json_decode(Configuration::get('LAST_SEARCH'),true);
                $order_by = explode('.', Tools::getValue('order'))[1];
                $order_way = explode('.', Tools::getValue('order'))[2];
                $this->searchs = $this->search($this->last_search_values['tipe_of_search'],(int)$this->last_search_values['category'],(int)$this->last_search_values['tipo'],(int)$this->last_search_values['larghezza'],(int)$this->last_search_values['lunghezza'],(int)$this->last_search_values['altezza'],$this->last_search_values['unit'],$order_by,$order_way);
            try {
                $this->product_listing->products_var = $this->searchs;
                $this->product_listing->id_categoria = (int)$this->last_search_values['category'];
                $this->listing = $this->product_listing->getProductSearchVariables();
            } catch (PrestaShopException $e) {
            }
        }
        parent::initContent();
        if (Tools::version_compare(_PS_VERSION_,'1.7.6','>='))
            $this->setMedia(false);
        else
            $this->setMedia();

        $this->context->smarty->assign('listing',$this->listing,true);
        $this->context->smarty->assign('searchproducts',1,true);
        $this->setTemplate('catalog/listing/product-list.tpl');

    }

    /**
     * @method searchLegacy() for compatibility width attribute group Dimension (20x40x60cm) format
     * @param null $tipo
     * @param null $larghezza
     * @param null $lunghezza
     * @param null $altezza
     * @param null $unit
     *
     * @return array|bool
     * @throws PrestaShopDatabaseException
     */
    public function searchLegacy($category,$tipo = null,$larghezza = null,$lunghezza = null,$altezza = null,$unit=null){

        $sql = 'SELECT * FROM `op_searchproducts` WHERE id='.$tipo.' ;';
        $result = '';
        if ($tipo!=null && $tipo > 0){
            $result = Db::getInstance()->executeS($sql);
        }

        $products = Product::getProducts(Context::getContext()->language->id,0,0,'id_product','ASC',$category,true);
        foreach ($products as $key => $product) {
            $match = false;
            foreach (Product::getAttributesInformationsByProduct((int)$product['id_product']) as $attribute) {
                if ($attribute['id_attribute_group'] == $this->attribute_group_dimension){
                    $product_dimentions = explode('x',str_replace('cm','',$attribute['attribute']));
                    if (count($result)>0){
                       $match = (int)$result[0]['width'] >= (int)$product_dimentions[0] && (int)$result[0]['height'] >= (int)$product_dimentions[1] && (int)$result[0]['depth'] >= (int)$product_dimentions[2];
                    }else{
                        $match = (int)$larghezza >= (int)$product_dimentions[0] && (int)$lunghezza >= (int)$product_dimentions[1] && (int)$altezza >= (int)$product_dimentions[2];
                    }

                    if(strpos($attribute['attribute'],($unit!=null?$unit:$result[0]['unit']))===false){
                        $match = false;
                    }
                }

            }
            if(!$match){
                unset($products[$key]);
            }
        }

        if (count($products)>0)
            return $products;

        return false;

    }

    /**
     * @method search()
     * @param null $tipo
     * @param null $larghezza
     * @param null $lunghezza
     * @param null $altezza
     * @param null $unit
     *
     * @return array|bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function search($tipe_of_search = 'box',$category,$tipo = null,$larghezza = null,$lunghezza = null,$altezza = null,$unit=null,$order_by = 'name', $order_way = 'asc'){

        $or_values = array();
        $count_valori=0;
        if ($larghezza){
            $count_valori++;
            $margin_down = max($larghezza-$this->search_margin ,1);
            $margin_up = $larghezza+$this->search_margin;
            $or_values[]='(a.id_attribute_group='.$this->attribute_group_width.' AND al.name BETWEEN '.$margin_down.' AND '.$margin_up.')';
        }
        if ($lunghezza){
            $count_valori++;
            $margin_down = max($lunghezza-$this->search_margin ,1);
            $margin_up = $lunghezza+$this->search_margin;
            $or_values[]='(a.id_attribute_group='.$this->attribute_group_height.' AND al.name BETWEEN '.$margin_down.' AND '.$margin_up.')';
        }
        if ($altezza){
            $count_valori++;
            $margin_down = max($altezza-$this->search_margin ,1);
            $margin_up = $altezza+$this->search_margin;
            $or_values[]='(a.id_attribute_group='.$this->attribute_group_depth.' AND al.name BETWEEN '.$margin_down.' AND '.$margin_up.')';
        }

        $sql = 'SELECT DISTINCT pa.`id_product`, pa.`id_product_attribute`,a.id_attribute,a.id_attribute_group,al.name
                FROM `' . _DB_PREFIX_ . 'product_attribute` pa
                    INNER JOIN
                    `' . _DB_PREFIX_ . 'product` p
                    ON (pa.id_product=p.id_product)
                    INNER JOIN 
                    `' . _DB_PREFIX_ . 'category_product` pcp
                    ON (p.id_product=pcp.id_product)
                    INNER JOIN 
                    `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                    ON (pac.id_product_attribute=pa.id_product_attribute)
                    INNER JOIN 
                    `' . _DB_PREFIX_ . 'attribute` a
                    ON (pac.id_attribute=a.id_attribute)
                    INNER JOIN 
                    `' . _DB_PREFIX_ . 'attribute_lang` al
                    ON (a.id_attribute=al.id_attribute)
                WHERE pcp.id_category='.$category.' and ('.implode(' OR ',$or_values).');';

        $products = Db::getInstance()->executeS($sql);
        $repeat_attributes_ids = array_count_values(array_column($products,'id_product_attribute'));
        $products_match = array();
        foreach ($products as $key => $product) {
            if ($repeat_attributes_ids[$product['id_product_attribute']]<$count_valori) // At least 3 coincidences
                continue;

            if (!isset($products_match[$product['id_product_attribute']]['dimensions']))
                $products_match[$product['id_product_attribute']]['dimensions'] = $this->getDefaultDimensionsByProductAttributeId($product['id_product_attribute']);

            $products_match[$product['id_product_attribute']]['id_product'] = $product['id_product'];
            $products_match[$product['id_product_attribute']]['id_product_attribute'] = $product['id_product_attribute'];
            $products_match[$product['id_product_attribute']]['quantity'] = Product::getQuantity($product['id_product']); //set total quantity
            $products_match[$product['id_product_attribute']]['id_category_default'] = $category;

            $products_match[$product['id_product_attribute']]['dimensions'][$this->getAttributeGroupNameById($product['id_attribute_group'])] = $product['name'];
            $products_match[$product['id_product_attribute']]['dimensions_name'] = $this->convertDimensionsToNameFormat($products_match[$product['id_product_attribute']]['dimensions']);
            $products_match[$product['id_product_attribute']]['category'] = Category::getCategoryInformation([$category])[$category]['name'];
            $products_match[$product['id_product_attribute']]['category_name'] = ucfirst(Category::getCategoryInformation([$category])[$category]['name']);
            $link = new Link();
            $products_match[$product['id_product_attribute']]['url'] = $link->getCombinationLink($product['id_product'],$product['id_product_attribute']);
            $products_match[$product['id_product_attribute']]['link'] = $link->getCombinationLink($product['id_product'],$product['id_product_attribute']);
            $products_match[$product['id_product_attribute']]['id_image'] = Product::getCover($product['id_product'])['id_image'];
        }

        if (count($products_match)>0)
            return $products_match;

        return false;

    }

    /**
     * implode dimensions
     * @param $dimensions
     * @return string 1x1x1 cm.
     */
    public function convertDimensionsToNameFormat($dimensions){
       return implode('x',array_values($dimensions)).' cm.';
    }

    public function getDefaultDimensionsByProductAttributeId($id_product_attribute){
        $result = Db::getInstance()->executeS('
        SELECT DISTINCT a.`id_attribute`, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`,pa.`reference`
        FROM `' . _DB_PREFIX_ . 'attribute` a
        LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al
            ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) Context::getContext()->language->id . ')
        LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl
            ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = ' . (int) Context::getContext()->language->id . ')
        LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac
            ON (a.`id_attribute` = pac.`id_attribute`)
        LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
            ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
        ' . Shop::addSqlAssociation('product_attribute', 'pa') . '
        ' . Shop::addSqlAssociation('attribute', 'pac') . '
        WHERE pa.`id_product_attribute` = '. (int) $id_product_attribute.' 
        AND (a.`id_attribute_group`=' . (int) $this->attribute_group_height.' OR a.`id_attribute_group`=' . (int) $this->attribute_group_width.' OR a.`id_attribute_group`=' . (int) $this->attribute_group_depth.');');

        $dimensions = array('Lunghezza'=> 0,
                            'Larghezza'=> 0,
                            'Altezza'=> 0);

        foreach ($result as $attribute){
            switch ($attribute['id_attribute_group']){
                case $this->attribute_group_width:
                    $dimensions['Larghezza'] = $attribute['attribute'];
                    break;
                case $this->attribute_group_height:
                    $dimensions['Lunghezza'] = $attribute['attribute'];
                    break;
                case $this->attribute_group_depth:
                    $dimensions['Altezza'] = $attribute['attribute'];
                    break;
            }
        }
        return $dimensions;
    }

    /**
     * @param $id_attribute_group
     * @return string
     */
    public function getAttributeGroupNameById($id_attribute_group){
        $name='';
        switch ($id_attribute_group){
            case $this->attribute_group_width:
                $name = 'Larghezza';
                break;
            case $this->attribute_group_height:
                $name = 'Lunghezza';
                break;
            case $this->attribute_group_depth:
                $name = 'Altezza';
                break;
        }
        return $name;
    }

}