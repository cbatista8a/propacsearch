/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
window.onload = function () {
  let tipos = $('#tipo_modal_form_box #select_tipo')[0];
      $(tipos).on('change',function () {
        debugger;
        if (this.value === '0'){
          document.getElementById('fields-group-2').style.display = 'block';
        }else {
          document.getElementById('fields-group-2').style.display = 'none';
        }
    });
      $('#radio_box').on('change',function () {
        if($(this).is(':checked')){
          document.getElementById('tipo_modal_form_box').style.display = 'block';
          document.getElementById('tipo_modal_form_bags').style.display = 'none';
        }

      });
      $('#radio_bags').on('change',function () {
        if($(this).is(':checked')){
          document.getElementById('tipo_modal_form_bags').style.display = 'block';
          document.getElementById('tipo_modal_form_box').style.display = 'none';
        }

      });

      //for order in custom result page by facets
  if (window.location.href.indexOf("orangesearch") != -1){
      let order_select = document.getElementsByClassName('products-sort-order')[0].querySelectorAll('a');
          $(order_select).each(function () {
              $(this).on('click',function (e) {
                debugger;
                e.stopPropagation();
                window.location.href = this.href;
              });
          });
  }
};