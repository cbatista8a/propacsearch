/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
window.onload = function () {

  $(document).ready(function() {

    /**
     * Register events on buttons
     */
    $('.tipo_update').on('click',function () {
      let item = this.parentNode.parentNode;
      updateItem(item);
    });
    $('.tipo_delete').on('click',function () {
      let item = this.parentNode.parentNode;
      deleteItem(item);
    });

    /**
     * Send ajax form and save new typology
     */
    $('#tipo_modal_form').on('submit', function (e) {
      e.preventDefault();
      let data =new FormData(this);
      $.ajax({
        type: "POST",
        url: this.action,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {
          refreshList(response);
        },
        error: function (error) {
          alert(error);
        }

        });
    });
    /**
     * Send ajax form and save product category
     */
    $('#category_modal_form').on('submit', function (e) {
      e.preventDefault();
      let data =new FormData(this);
      $.ajax({
        type: "POST",
        url: this.action,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {

        },
        error: function (error) {
          alert(error);
        }

        });
    });

  });


};

function updateItem(item) {
  let url = document.getElementById('tipo_modal_form').action;
  let inputs = item.querySelectorAll('input');
  let id = inputs[0].value;
  let tipo_width = inputs[1].value;
  let tipo_height = inputs[2].value;
  let tipo_depth = inputs[3].value;
  let tipo_unit = item.querySelector('select').value;
  let data =new FormData();
  data.append('update_tipo','true');
  data.append('id_tipo',id);
  data.append('tipo_width',tipo_width);
  data.append('tipo_height',tipo_height);
  data.append('tipo_depth',tipo_depth);
  data.append('tipo_unit',tipo_unit);
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshList(response);
    },
    error: function (error) {
      alert(error);
    }

  });
}

function deleteItem(item) {
  let url = document.getElementById('tipo_modal_form').action;
  let id = item.querySelector('.id_tipo').value;
  let data =new FormData();
  data.append('delete_tipo','true');
  data.append('id_tipo',id);
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshList(response);
    },
    error: function (error) {
      alert(error);
    }

  });
}

function refreshList(html) {
  $('.list-container').html(html);

  $('.tipo_update').on('click',function () {
    let item = this.parentNode.parentNode;
    updateItem(item);
  });
  $('.tipo_delete').on('click',function () {
    let item = this.parentNode.parentNode;
    deleteItem(item);
  });
}

