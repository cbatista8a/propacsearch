<ul class="list-group mt-3">
    {foreach from=$tipos item=$tipo}
        <li class="card list-group-item m-1">

            <div class="row toggle card-title" id="dropdown-detail-1" data-toggle="collapse" aria-expanded="false" data-target="#detail-{$tipo.id}" aria-controls="detail-{$tipo.id}">
                <div class="col-10">
							{$tipo.typo_name}
						</div>
                <div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
            </div>
            <div id="detail-{$tipo.id}" class="card-body collapse">
                <div class="row">

                    <div class="m-1 col-1">
                        <span>{l s='Larghezza' mod='op_searchproducts'}: </span><input type="number" name="tipo_width" value="{$tipo.width}">
                    </div>
                    <div class="m-1 col-1">
                        <span>{l s='Lunghezza' mod='op_searchproducts'}: </span><input type="number" name="tipo_height" value="{$tipo.height}">
                    </div>
                    <div class="m-1 col-1">
                        <span>{l s='Altezza' mod='op_searchproducts'}: </span><input type="number" name="tipo_depth" value="{$tipo.depth}">
                    </div>
                    <div class="m-1 col-1">
                        <span>{l s='Unità di misura' mod='op_searchproducts'}: </span>
                        <select  name="tipo_unit">
                            <option value="mm" {if $tipo.unit == 'mm'}selected{/if} disabled>mm</option>
                            <option value="cm" {if $tipo.unit == 'cm'}selected{/if}>cm</option>
                            <option value="m" {if $tipo.unit == 'm'}selected{/if} disabled>m</option>
                        </select>
                    </div>
                    <div class="btn_actions col-7 d-flex align-items-center justify-content-end">
                        <input type="hidden" class="id_tipo" name="id_tipo" value="{$tipo.id}">
                        <button class="btn btn-primary m-1 tipo_update">{l s='Aggiorna' mod='op_searchproducts'}</button>
                        <button class="btn btn-danger m-1 tipo_delete">{l s='Cancella' mod='op_searchproducts'}</button>
                    </div>

                </div>
            </div>
        </li>
    {/foreach}
</ul>