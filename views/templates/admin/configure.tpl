{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
{**}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->
<div class="panel">
    <h3><i class="icon icon-search"></i> {l s='Configura il modulo' mod='op_searchproducts'}</h3>
    <form method="post" id="category_modal_form" action="{$action_form}">
        <input type="hidden" name="form_category_save">
        <div class="form-group row">
            <span>{l s='Categoria Scatola' mod='op_searchproducts'}: </span>
            <select value="" name="category-scatola" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$categories item=cat}
                    <option value="{$cat.id_category}" {if isset($category_scatole) && $category_scatole==$cat.id_category}selected{/if}>{$cat.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row">
            <span>{l s='Lunghezza' mod='op_searchproducts'}: </span>
            <select value="" name="attribute-group-height" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$attribute_groups item=a_group}
                    <option value="{$a_group.id_attribute_group}" {if isset($group_height) && $group_height==$a_group.id_attribute_group}selected{/if}>{$a_group.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row">
            <span>{l s='Larghezza' mod='op_searchproducts'}: </span>
            <select value="" name="attribute-group-width" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$attribute_groups item=a_group}
                    <option value="{$a_group.id_attribute_group}" {if isset($group_width) && $group_width==$a_group.id_attribute_group}selected{/if}>{$a_group.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row">
            <span>{l s='Altezza' mod='op_searchproducts'}: </span>
            <select value="" name="attribute-group-depth" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$attribute_groups item=a_group}
                    <option value="{$a_group.id_attribute_group}" {if isset($group_depth) && $group_depth==$a_group.id_attribute_group}selected{/if}>{$a_group.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row border-top pt-3">
            <span>{l s='Categoria Sacchetti' mod='op_searchproducts'}: </span>
            <select value="" name="category-sacchetti" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$categories item=cat}
                    <option value="{$cat.id_category}" {if isset($category_sacchetti) && $category_sacchetti==$cat.id_category}selected{/if}>{$cat.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row">
            <span>{l s='Lunghezza Sacchetti' mod='op_searchproducts'}: </span>
            <select value="" name="attribute-group-height-sacchetti" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$attribute_groups item=a_group}
                    <option value="{$a_group.id_attribute_group}" {if isset($height_sacchetti) && $height_sacchetti==$a_group.id_attribute_group}selected{/if}>{$a_group.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row">
            <span>{l s='Larghezza Sacchetti' mod='op_searchproducts'}: </span>
            <select value="" name="attribute-group-width-sacchetti" class="col-4 ml-1">
                <option value="0">{l s='Sceglie...' mod='op_searchproducts'}</option>
                {foreach from=$attribute_groups item=a_group}
                    <option value="{$a_group.id_attribute_group}" {if isset($width_sacchetti) && $width_sacchetti==$a_group.id_attribute_group}selected{/if}>{$a_group.name}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group row border-top pt-3">
            <span>{l s='Margine di ricerca' mod='op_searchproducts'}: </span>
            <input type="number" class="col-1 ml-1" name="search-margin" value="{$search_margin}" max="10" min="0">
        </div>
        <div class="btn-group pull-right" role="group">
            <button type="submit" id="category_save" name="category_save" class="btn-primary btn-default btn-hover-green ml-3" >{l s='Salva' mod='op_searchproducts'}</button>
        </div>
    </form>
</div>

<div class="panel">
	<h3><i class="icon icon-search"></i> {l s='Ricerca scatole tramite dimensioni' mod='op_searchproducts'}</h3>
	<br/>
	<div class="p-3">

			<div class="row">
				<h3 class="col-10">{l s='Lista delle tipologie di prodotto' mod='op_searchproducts'}:</h3>
				<button class="btn btn-primary col-1" data-toggle="modal" data-target="#squarespaceModal">{l s='Aggiungi' mod='op_searchproducts'}</button>
			</div>
            <div class="list-container">
                {$list_tipos}
            </div>
	</div>

    <!-- line modal -->
    <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="lineModalLabel">{l s='Nuova Tipologia' mod='op_searchproducts'}</h3>
                    <button type="button" class="close pr-1" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">{l s='Chiudi' mod='op_searchproducts'}</span></button>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form method="post" id="tipo_modal_form" action="{$action_form}">
                        <input type="hidden" name="form_tipe_save">
                        <div class="form-group">
                            <label for="tipo_name">{l s='Nome Tipologia' mod='op_searchproducts'}</label>
                            <input type="text" class="form-control" id="tipo_name" name="tipo_name" placeholder="Nome (es. piccollo)" required>
                        </div>
                        <div class="form-group">
                            <label for="tipo_width">{l s='Larghezza' mod='op_searchproducts'}:</label>
                            <input type="number" class="form-control" id="tipo_width" name="tipo_width" placeholder="Larghezza" required>
                        </div>
                        <div class="form-group">
                            <label for="tipo_height">{l s='Lunghezza' mod='op_searchproducts'}:</label>
                            <input type="number" class="form-control" id="tipo_height" name="tipo_height" placeholder="Lunghezza" required>
                        </div>
                        <div class="form-group">
                            <label for="tipo_depth">{l s='Altezza' mod='op_searchproducts'}:</label>
                            <input type="number" class="form-control" id="tipo_depth" name="tipo_depth" placeholder="Profondità" required>
                        </div>
                        <div class="form-group">
                            <span>{l s='Unità di misura' mod='op_searchproducts'}: </span>
                            <select value="" name="tipo_unit">
                                <option value="mm" disabled>mm</option>
                                <option value="cm" selected>cm</option>
                                <option value="m" disabled>m</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">{l s='Chiudi' mod='op_searchproducts'}</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="submit" id="tipo_save" name="tipo_save" class="btn btn-default btn-hover-green" >{l s='Salva' mod='op_searchproducts'}</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <!--End line modal -->

</div>
