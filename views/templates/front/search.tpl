<div class="orangesearch center" id="orangesearch-box">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="lineModalLabel">{l s='Trova la tua scatola' mod='op_searchproducts'}</h3>
            </div>
            <div class="modal-body" style="padding: 10px;">
                <div class="radio-form-selector" style="width: 49%; float: left; display:none;">
                    {if $id_category != $category_sacchetti && !in_array($id_category,$category_childs)}<input id="radio_box" type="radio" name="tipo" checked value="box">{l s='Scatola' mod='op_searchproducts'}{/if}
                    {if $id_category == $category_sacchetti || in_array($id_category,$category_childs)}<input id="radio_bags" type="radio" name="tipo" {if $id_category == $category_sacchetti || in_array($id_category,$category_childs)}checked{/if} value="bags">{l s='Sacchetti' mod='op_searchproducts'}{/if}
                </div>
                <!-- content goes here -->
                {if $id_category != $category_sacchetti && !in_array($id_category,$category_childs)}
                    <form method="post" id="tipo_modal_form_box" action="{$search_action}">
                        {if isset($tipos)}
                            <div class="form-group" id="fields-group-1" style="width: 100%; float: left;">
                                <h4 class="">{l s='Cerca per Tipologia' mod='op_searchproducts'}</h4>
                                <select name="select_tipo" id="select_tipo" style="width: 100%">
                                    <option value="0" selected>{l s='Cerca la tipologia' mod='op_searchproducts'}</option>
                                    {foreach from=$tipos item=$tipo}
                                        <option value="{$tipo.id}">{$tipo.typo_name} (Max: {$tipo.height}x{$tipo.width}x{$tipo.depth}cm)</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/if}
                        {if $enable_dimensions}
                            <div id="fields-group-2" class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <h4 class="">{l s='Cerca per misure' mod='op_searchproducts'} (cm)</h4>
                                </div>
                                <div class="form-group col-lg-12 col-sm-12">
                                    <input type="number" class="form-control" id="tipo_height" name="tipo_height" placeholder="Lunghezza">
                                </div>
                                <div class="form-group col-lg-12 col-sm-12">
                                    <input type="number" class="form-control" id="tipo_width" name="tipo_width" placeholder="Larghezza">
                                </div>
                                <div class="form-group col-lg-12 col-sm-12">
                                    <input type="number" class="form-control" id="tipo_depth" name="tipo_depth" placeholder="Altezza">
                                </div>
                            </div>
                        {/if}
                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <input type="hidden" class="form-control" name="form_box" value="box">
                                    <button type="submit" id="tipo_save" name="tipo_save" class="btn btn-default btn-hover-green">{l s='Cerca' mod='op_searchproducts'}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                {/if}
                {if $id_category == $category_sacchetti || in_array($id_category,$category_childs)}
                    <form method="post" id="tipo_modal_form_bags" action="{$search_action}" {if $id_category != $category_sacchetti && !in_array($id_category,$category_childs)}style="display: none;"{/if}>
                        {if $enable_dimensions}
                            <div id="fields-group-2" class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <h4 class="">{l s='Cerca per Misure' mod='op_searchproducts'}(cm)</h4>
                                </div>
                                <div class="form-group col-lg-6 col-sm-12">
                                    <label for="tipo_width_bags">{l s='Larghezza' mod='op_searchproducts'}:</label>
                                    <input type="number" class="form-control" id="tipo_width_bags" name="tipo_width" placeholder="20">
                                </div>
                                <div class="form-group col-lg-6 col-sm-12">
                                    <label for="tipo_height_bags">{l s='Lunghezza' mod='op_searchproducts'}:</label>
                                    <input type="number" class="form-control" id="tipo_height_bags" name="tipo_height" placeholder="25">
                                </div>
                            </div>
                        {/if}
                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <input type="hidden" class="form-control" name="form_bags" value="bags">
                                    <button type="submit" id="tipo_save" name="tipo_save" class="btn btn-default btn-hover-green">{l s='Cerca' mod='op_searchproducts'}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                {/if}
            </div>
        </div>
    </div>
</div>
