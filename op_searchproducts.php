<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_searchproducts extends Module
{
    protected $config_form = false;
    protected $templateFile;

    public function __construct()
    {
        $this->name = 'op_searchproducts';
        $this->tab = 'search_filter';
        $this->version = '1.0.0';
        $this->author = 'OrangePix Srl';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('OrangePix Srl - Ricerca scatole tramite dimensioni');
        $this->description = $this->l('Ricerca scatole per tipologia o dimensioni');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:op_searchproducts/views/templates/front/search.tpl';

    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
          $this->registerHook('header') &&
          $this->registerHook('backOfficeHeader') &&
          $this->registerHook('top') &&
          $this->registerHook('displayHeader');
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');
        Configuration::deleteByName('CATEGORY_SCATOLE_SEARCH');
        Configuration::deleteByName('CATEGORY_SACCHETTI_SEARCH');
        Configuration::deleteByName('ATTRIBUTE_GROUP_WIDTH');
        Configuration::deleteByName('ATTRIBUTE_GROUP_HEIGHT');
        Configuration::deleteByName('ATTRIBUTE_GROUP_DEPTH');
        Configuration::deleteByName('SEARCH_MARGIN');
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('form_tipe_save')) == true) {
            $this->saveScatoleTipo();
        }
        if (((bool)Tools::isSubmit('update_tipo')) == true) {
            $this->updateSacatoleTipo();
        }
        if (((bool)Tools::isSubmit('delete_tipo')) == true) {
            $this->deleteSacatoleTipo();
        }
        if (((bool)Tools::isSubmit('form_category_save')) == true) {
            $this->postProcess();
        }


        $token = Tools::getAdminTokenLite('AdminModules');
        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('action_form', $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token);
        $this->context->smarty->assign('list_tipos', $this->listScatoleTiposHtml());
        $this->context->smarty->assign('categories', Category::getSimpleCategories($this->context->language->id));
        $this->context->smarty->assign('category_scatole',(int)Configuration::get('CATEGORY_SCATOLE_SEARCH') );
        $this->context->smarty->assign('category_sacchetti',(int)Configuration::get('CATEGORY_SACCHETTI_SEARCH') );
        $this->context->smarty->assign('attribute_groups', AttributeGroup::getAttributesGroups($this->context->language->id));
        $this->context->smarty->assign('group_width',(int)Configuration::get('ATTRIBUTE_GROUP_WIDTH') );
        $this->context->smarty->assign('group_height',(int)Configuration::get('ATTRIBUTE_GROUP_HEIGHT') );
        $this->context->smarty->assign('group_depth',(int)Configuration::get('ATTRIBUTE_GROUP_DEPTH') );
        $this->context->smarty->assign('width_sacchetti',(int)Configuration::get('ATTRIBUTE_GROUP_WIDTH_SACCHETTI') );
        $this->context->smarty->assign('height_sacchetti',(int)Configuration::get('ATTRIBUTE_GROUP_HEIGHT_SACCHETTI') );
        $this->context->smarty->assign('search_margin',(int)Configuration::get('SEARCH_MARGIN') );

        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
    }

    /**
     * Get a list of Box Types
     * @throws PrestaShopDatabaseException
     */
    public function listScatoleTiposHtml(){

        $sql = 'SELECT * FROM `op_searchproducts`;';
        $html = '';
        $result = Db::getInstance()->executeS($sql);

        if (count($result)>0){
            $this->context->smarty->assign('tipos', $result);
            $html = $this->context->smarty->fetch($this->local_path.'views/templates/admin/list_tipos.tpl');
        }
        return $html; //return html response
    }

    /**
     * Save new Box Type
     * @throws PrestaShopDatabaseException
     */
    public function saveScatoleTipo(){
        $data['typo_name'] = Tools::getValue('tipo_name');
        $data['width'] = Tools::getValue('tipo_width');
        $data['height'] = Tools::getValue('tipo_height');
        $data['depth'] = Tools::getValue('tipo_depth');
        $data['unit'] = Tools::getValue('tipo_unit');
        $result = Db::getInstance()->insert($this->name,$data,false,false,Db::INSERT,false);
        if ($result){
            $result = $this->listScatoleTiposHtml();
        }
        die($result);
    }

    /**
     * Update Box Type values
     * @throws PrestaShopDatabaseException
     */
    public function updateSacatoleTipo(){
        $id = Tools::getValue('id_tipo');

        $data['width'] = Tools::getValue('tipo_width');
        $data['height'] = Tools::getValue('tipo_height');
        $data['depth'] = Tools::getValue('tipo_depth');
        $data['unit'] = Tools::getValue('tipo_unit');
        $result = Db::getInstance()->update($this->name,$data, 'id='.$id,0,false,true,false);
        if ($result){
            $result = $this->listScatoleTiposHtml();
        }

        die($result);

    }

    /**
     * Delete Box Type values
     * @throws PrestaShopDatabaseException
     */
    public function deleteSacatoleTipo(){
        $id = Tools::getValue('id_tipo');
        $result = Db::getInstance()->delete($this->name, 'id='.$id,0,false,false);
        if ($result){
            $result = $this->listScatoleTiposHtml();
        }

        die($result);

    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        Configuration::updateValue('CATEGORY_SCATOLE_SEARCH', Tools::getValue('category-scatola'));
        Configuration::updateValue('CATEGORY_SACCHETTI_SEARCH', Tools::getValue('category-sacchetti'));
        Configuration::updateValue('ATTRIBUTE_GROUP_WIDTH', Tools::getValue('attribute-group-width'));
        Configuration::updateValue('ATTRIBUTE_GROUP_HEIGHT', Tools::getValue('attribute-group-height'));
        Configuration::updateValue('ATTRIBUTE_GROUP_DEPTH', Tools::getValue('attribute-group-depth'));
        Configuration::updateValue('ATTRIBUTE_GROUP_WIDTH_SACCHETTI', Tools::getValue('attribute-group-width-sacchetti'));
        Configuration::updateValue('ATTRIBUTE_GROUP_HEIGHT_SACCHETTI', Tools::getValue('attribute-group-height-sacchetti'));
        Configuration::updateValue('SEARCH_MARGIN', Tools::getValue('search-margin'));
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookTop()
    {
        return $this->renderSearchForm();
    }

    public function renderWidget()
    {
        return $this->renderSearchForm();
    }

    private function renderSearchForm() {
        $category_scatole_casse = 519; //Scatole e Casse
        $category_childs_scatole_casse = Category::getChildren($category_scatole_casse, $this->context->language->id);
        $category_childs_scatole_casse = array_column($category_childs_scatole_casse,'id_category');
        if (!in_array(Tools::getValue('controller'),['index','category'])){
            return ;
        }
        if (Tools::isSubmit('id_category') && ((int)Tools::getValue('id_category') !== $category_scatole_casse && !in_array((int)Tools::getValue('id_category'), $category_childs_scatole_casse, false))){
            return ;
        }

        $sql = 'SELECT * FROM `op_searchproducts`;';
        $result = Db::getInstance()->executeS($sql);
        if (count($result)>0){
            $this->smarty->assign('tipos', $result);
        }
        $enable_dimensions = true;
        if ((int)Configuration::get('ATTRIBUTE_GROUP_WIDTH') == 0 && (int)Configuration::get('ATTRIBUTE_GROUP_HEIGHT') == 0 && (int)Configuration::get('ATTRIBUTE_GROUP_DEPTH') == 0){
            $enable_dimensions=false;
        }
        $this->smarty->assign('enable_dimensions', $enable_dimensions);
        $category_childs = Category::getChildren((int)Configuration::get('CATEGORY_SACCHETTI_SEARCH'),$this->context->language->id);
        $category_childs = array_column($category_childs,'id_category');
        $this->smarty->assign('category_sacchetti', (int)Configuration::get('CATEGORY_SACCHETTI_SEARCH'));
        $this->smarty->assign('category_childs', $category_childs);
        $this->smarty->assign('id_category', isset($this->context->smarty->tpl_vars['id_category']) ? $this->context->smarty->tpl_vars['id_category'] : 'home' );
        $this->smarty->assign('module_dir', $this->_path);
        $this->smarty->assign('search_action',$action= $this->context->link->getModuleLink($this->name,'orangesearch'));
        return $this->fetch($this->templateFile);
    }
}
